const express = require('express');
const router = express.Router();

const auth = require("../auth");
const {verify, verifyAdmin} = auth;


// import user controllers

const userControllers = require("../controllers/userControllers");

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve ALL Users
router.get("/", userControllers.getAllUsers);

// Login
router.post("/login", userControllers.loginUser)

// Retrieving User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails)

// Check Email
router.post("/checkEmailExist/", userControllers.checkEmailExist)

// Update User to Admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin)

// Update User Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails)

// Enroll registered user
router.post("/enroll", verify, userControllers.enroll);


router.get("/getEnrollments", verify, userControllers.getEnrollments);

module.exports = router;