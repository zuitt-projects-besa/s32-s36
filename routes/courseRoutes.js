const express = require('express');
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

// object destructuring in js
const {verify, verifyAdmin} = auth;

router.post('/', verify, verifyAdmin, courseControllers.addCourse);

router.get('/', courseControllers.getAllCourse);

router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

router.put("/archive/:id", verify, verifyAdmin, courseControllers.archive);

router.put("/activate/:id", verify, verifyAdmin, courseControllers.activate);

router.get("/getActiveCourses", courseControllers.getActiveCourses);

router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

router.get("/getInactiveCourses",verify, verifyAdmin, courseControllers.getInactiveCourses);

// find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName)

// find course by price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice)

router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees)

module.exports = router;
