const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

/*
	Notes:

	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow acces to a certain parts of our app.

	JWT is like a gifst wrapping service that is able to encode our user details which can only be uwrapped by jwt's own method and if the secret provided is intact.

	IF the JWT seemed tampered with, we will reject the users attemp to access a feature
*/

module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}

	return jwt.sign(data, secret, {})
};

/*
	Notes:

	>> You can only get a unique jwt with our secret if you log in to our app with the correct email and password

	>> As a user, you can only get your own details from your own tokem from logging in

	>> JWT is not meant to store sensitive data. For now, for ease of use and for our MVP (minimum viable product), we add the email and isAdmin details of the logged in user. However, in the future, you can limit this to only id and for every route and feature, you can simply look up for the user in the database to get his details

	>>We will verify the legitimacy of a JWT everytime a user access a restricted feature. Each JWT contains a secret only our server knows. IF the jwt has been changed in any way, we will reject the use and his tampered token. IF the jwt does not contain a secret OR the secret is different, we will reject his access and token
*/

module.exports.verify = (req, res, next) => {
	// req.headers.authorization contains sensitive data and especially our token/jwt
	let token = req.headers.authorization
	console.log(token)

	if (typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"})
	} else {
		token = token.slice(7, token.length)
		// Bearer eyjhgasdjhJHJhgjhazxc

		jwt.verify(token, secret, (err, decodedToken) => {
			if (err) {
				return res.send({
					auth: "Failed",
					message: err.message
				})

			} else {
				req.user = decodedToken;

				next();
			}
		})
	}};

module.exports.verifyAdmin = (req, res, next) => {

	if (req.user.isAdmin) {
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Forbidden Action"
		})
	}

};