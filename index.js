const express = require('express');
const mongoose = require('mongoose');

// allows our backend application to be available in our frontend application
// cors - cross origin resource sharing
const cors = require('cors');

const port = 4000;
const app = express();

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");


// Connect DB
mongoose.connect("mongodb+srv://admin_besa:admin169@besa-169.lzfjc.mongodb.net/bookingAPI169?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Notif
let db = mongoose.connection;
db.on('error', console.error.bind(console,'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

// Middleware
app.use(express.json());
app.use(cors());

app.use('/users', userRoutes);
app.use('/courses', courseRoutes);



app.listen(port, () => console.log(`Server is running at port ${port}`));