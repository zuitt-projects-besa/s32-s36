const mongoose = require ('mongoose');

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required:[true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Number is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "userID is required"]
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Course", courseSchema)
