// import bcrypt module
const bcrypt = require("bcrypt");

// import User model
const User = require("../models/User");

// import Course model

const Course = require("../models/Course")

// import auth module
const auth = require("../auth");

// Controllers

// User Registration
module.exports.registerUser = (req, res) => {

	console.log(req.body);

	/*
		bcrypt - add a layer of security to your user's password

		What bcrypt does is hash our password into a randomized character version of the original string

		syntax: bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

		Salt-Rounds are the number of times the characters in the has are randomized
	*/

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// Create a new user documents out of our user model

	let newUser = new User ({

		firstName : req.body.firstName,
		lastName : req.body.lastName,
		email : req.body.email,
		mobileNo : req.body.mobileNo,
		password : hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error));
};

// Retrieve All Users
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

// Login
module.exports.loginUser = (req, res) => {
	console.log(req.body);

	/*
		1. find by user email
		2. if user email is found, check the password
		3. if we don't find the user email, send a message
		4. if uppon checking the found user's password is the same as our input password, we will now generate a "key" to access our app, If not, we will turn him away by sending a message to the client
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null) {
			return res.send("User does not exist");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if (isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Password is incorrect")
			}
		}
	})
	.catch(err => res.send(err));
};

// Get User Details
module.exports.getUserDetails = (req, res) => {

	console.log(req.user)

	// 1. find a logged in user's document from out db and sent it to the client by its id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

module.exports.checkEmailExist = (req, res) => {
	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if (foundEmail === null) {
			return res.send("Email is available");
		} else {
			return res.send("Email is already registered");
		}
	})
	.catch(err => res.send(err));
};

// Updating a regular user to admin
module.exports.updateAdmin = (req, res) => {

	// req.user.id captures the id of the logged in admin
	console.log(req.user.id);

	// req.params.id captures the id designated in the params
	console.log(req.params.id);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedUser => res.send(updatedUser))
	.catch(errpr => res.send(error));
};

module.exports.updateUserDetails = (req, res) => {
	
	console.log(req.body); //checking the input for new values for our user's details
	console.log(req.user.id); //check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
	}

	User.findByIdAndUpdate(req.user.id, updates, {new : true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));

};

// Enroll a user

module.exports.enroll = async (req, res) => {

	/*
		Enrollmet Process:

		1. Look for the user by its id
			>> push the details of the course we're trying to enroll in.
				>> we'll push to a new enrollment subdocument in our user
		2. Look for the course by its id.
			>> push the details of the enrollee/user who's trying to enroll.
				>> we'll push to a new enrollees subdocument in our course
		3. When both saving documents are successfull, we send a message to the client
	*/

	console.log(req.user.id);
	console.log(req.body.courseId);

	// Check if a user is an admin. Admin cant enroll
	if (req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

	/*
		Find the user:

		async - a keyword that allows us to make our function asynchronous. Which means, that instead of JS regular behavior of running each code line by line, it will allow us to wait for the result of the function

		await - a keyword that allows us to wait for the function to finish before proceeding
	*/

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {
		console.log(user);

		// add the courseId in an object and push that into user's enrollment array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err => err.message);
	})

	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message
	if (isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		console.log(course);

		// create an object which will contain the user id of the enrollee of a course
		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee)

		return course.save().then(course => true).catch(err => err.message)
	})

	if (isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if (isUserUpdated && isCourseUpdated) {
		return res.send({message: 'Enrolled Successfully!'})
	}

};



module.exports.getEnrollments = (req, res) => {

	User.findById(req.user.id)
	.then(result => {
		console.log(result.enrollments)
		res.send(result.enrollments)
	})
	.catch(error => res.send(error));

};
