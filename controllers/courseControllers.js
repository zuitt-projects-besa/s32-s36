const Course = require("../models/Course");


module.exports.addCourse = (req, res) => {
	console.log(req.body);

	let newCourse = new Course ({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err))
};

module.exports.getAllCourse = (req, res) => {
	Course.find({})
	.then(course => res.send(course))
	.catch(err => res.send(err))
};


// localhost:4000/courses/getSingleCourse/624563be7d6cecee1f1b2141

module.exports.getSingleCourse = (req, res) => {
	Course.findById(req.params.id)

	.then(result => res.send(result))
	.catch(result => res.send(error));
};


module.exports.archive = (req,res) => {
	console.log(req.params.id)

	updates = {
		isActive : false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updateActive => res.send(updateActive))
	.catch(error => res.send(error))
}

module.exports.activate = (req,res) => {
	console.log(req.params.id)

	updates = {
		isActive : true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updateActive => res.send(updateActive))
	.catch(error => res.send(error))
}

module.exports.getActiveCourses = (req, res) => {

	Course.find({isActive : true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.updateCourse = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	};

	Course.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));
};

module.exports.getInactiveCourses = (req, res) => {

	Course.find({isActive : false})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

// find courses by name
module.exports.findCoursesByName = (req, res) => {
	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then (result => {
		if (result.length === 0) {
			return res.send("No courses found")
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
};

// find courses by price

module.exports.findCoursesByPrice = (req, res) => {
	Course.find({price: req.body.price})
	.then (result => {
		if (result.length === 0){
			return res.send("No course found")
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
};

module.exports.getEnrollees = (req, res) => {

	Course.findById(req.params.id)
	.then(result => {
		console.log(result.enrollees)
		res.send(result.enrollees)
	})
	.catch(error => res.send(error));

};
